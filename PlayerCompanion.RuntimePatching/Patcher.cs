﻿extern alias SHVDN3;

using HarmonyLib;
using JetBrains.Annotations;

namespace PlayerCompanion.RuntimePatching
{
    /// <summary>
    /// Patches the calls to Game.Player.Money in SHVDN 2 and 3.
    /// </summary>
    [UsedImplicitly]
    public class Patcher : SHVDN3::GTA.Script
    {
        public Patcher()
        {
#if DEBUG
#pragma  warning  disable S3010
            Harmony.DEBUG = true;
#endif
            var harmony = new Harmony("PlayerCompanion.RuntimePatching");
            harmony.PatchAll();
#if DEBUG
            FileLog.FlushBuffer();
#endif
        }
    }
}