﻿extern alias SHVDN2;
extern alias SHVDN3;

using HarmonyLib;
using JetBrains.Annotations;

namespace PlayerCompanion.RuntimePatching
{
    #region SHVDN2

    /// <summary>
    /// Patches for the Getter in SHVDN2.
    /// </summary>
    [HarmonyPatch(typeof(SHVDN2::GTA.Player))]
    [HarmonyPatch("Money", MethodType.Getter)]
    [UsedImplicitly]
    internal static class MoneyGetterPatch2
    {
        [UsedImplicitly]
        public static bool Prefix(ref int __result)
        {
            __result = Companion.Wallet.Money;
            return false;
        }
    }

    /// <summary>
    /// Patches for the Setter in SHVDN2.
    /// </summary>
    [HarmonyPatch(typeof(SHVDN2::GTA.Player))]
    [HarmonyPatch("Money", MethodType.Setter)]
    [UsedImplicitly]
    internal static class MoneySetterPatch2
    {
        [UsedImplicitly]
        public static bool Prefix(ref int value)
        {
            Companion.Wallet.Money = value;
            return false;
        }
    }

    #endregion SHVDN2

    #region SHVDN3

    /// <summary>
    /// Patches for the Getter in SHVDN3.
    /// </summary>
    [HarmonyPatch(typeof(SHVDN3::GTA.Player))]
    [HarmonyPatch("Money", MethodType.Getter)]
    [UsedImplicitly]
    internal static class MoneyGetterPatch3
    {
        [UsedImplicitly]
        public static bool Prefix(ref int __result)
        {
            __result = Companion.Wallet.Money;
            return false;
        }
    }

    /// <summary>
    /// Patches for the Setter in SHVDN3.
    /// </summary>
    [HarmonyPatch(typeof(SHVDN3::GTA.Player))]
    [HarmonyPatch("Money", MethodType.Setter)]
    [UsedImplicitly]
    internal static class MoneySetterPatch3
    {
        [UsedImplicitly]
        public static bool Prefix(ref int value)
        {
            Companion.Wallet.Money = value;
            return false;
        }
    }

    #endregion SHVDN3
}