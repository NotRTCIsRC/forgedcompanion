﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using fNbt;
using GTA;
using GTA.Native;
using GTA.UI;
using Newtonsoft.Json;

namespace PlayerCompanion
{
    /// <summary>
    /// Manages the money of the player.
    /// </summary>
    public class Wallet
    {
        #region Fields

        private readonly Dictionary<int, int> _moneyValues = new Dictionary<int, int>();

        #endregion Fields

        #region Properties

        /// <summary>
        /// The Money associated to the current Player Ped.
        /// </summary>
        public int Money
        {
            get => this[Game.Player.Character.Model];
            set => this[Game.Player.Character.Model] = value;
        }

        /// <summary>
        /// Gets or sets the money for a specific Ped Model.
        /// </summary>
        /// <param name="model">The Ped Model to check.</param>
        /// <returns>The money of the Ped.</returns>
        public int this[Model model]
        {
            get
            {
                if (model == PedHash.Michael || model == PedHash.Franklin || model == PedHash.Trevor)
                {
                    var stat = 0;
                    // ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
                    switch ((PedHash)model)
                    {
                        case PedHash.Michael:
                            stat = Game.GenerateHash("SP0_TOTAL_CASH");
                            break;

                        case PedHash.Franklin:
                            stat = Game.GenerateHash("SP1_TOTAL_CASH");
                            break;

                        case PedHash.Trevor:
                            stat = Game.GenerateHash("SP2_TOTAL_CASH");
                            break;
                    }

                    var result = 0;
                    unsafe
                    {
                        Function.Call(Hash.STAT_GET_INT, stat, &result, -1);
                    }
                    return result;
                }
                // ReSharper disable once RedundantIfElseBlock
                else
                {
                    return _moneyValues.ContainsKey(model) ? _moneyValues[model] : 0;
                }
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), "Money can't be under Zero.");
                }

                var difference = value - this[model];
                var positive = difference > 0;
                var sign = positive ? "+" : "-";
                Companion.MoneyChange.Text = $"{sign}${Math.Abs(difference)}";
                Companion.MoneyChange.Color = positive ? Color.LightGreen : Color.Red;
                Companion.DrawUntil = Game.GameTime + 5000;
                Companion.MoneyTotal.Text = $"${value}";

#pragma warning disable S2589 // Boolean expressions should not be gratuitous
                if (model == PedHash.Michael || model == PedHash.Franklin || model == PedHash.Franklin)
#pragma warning restore S2589 // Boolean expressions should not be gratuitous
                {
                    var stat = 0;
                    // ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
                    switch ((PedHash)model)
                    {
                        case PedHash.Michael:
                            stat = Game.GenerateHash("SP0_TOTAL_CASH");
                            break;

                        case PedHash.Franklin:
                            stat = Game.GenerateHash("SP1_TOTAL_CASH");
                            break;

                        case PedHash.Trevor:
                            stat = Game.GenerateHash("SP2_TOTAL_CASH");
                            break;
                    }
                    Function.Call(Hash.STAT_SET_INT, stat, value, 1);
                }
                else
                {
                    _moneyValues[model] = value;
                    Save();
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Creates a new Wallet.
        /// </summary>
        internal Wallet()
        {
            var file = Path.Combine(Companion.Location, "Money.nbt");
            // If there is a money file, try to load it
            if (File.Exists(file))
            {
                try
                {
                    using (var stream = File.OpenRead(file))
                    {
                        var reader = new NbtReader(stream);
                        var tag = reader.ReadAsTag();
                        if (tag["Version"].IntValue != 1)
                        {
                            throw new InvalidOperationException("Invalid NBT version!");
                        }

                        var compound = (NbtCompound)tag["Wallets"];
                        foreach (var item in compound.Tags)
                        {
                            _moneyValues[int.Parse(item.Name)] = item.IntValue;
                        }
                    }
                }
                catch (JsonSerializationException e)
                {
                    Notification.Show($"~r~Error~s~: Unable to load the Money File: {e.Message}");
                }
            }
            // Otherwise, create an empty file
            else
            {
                Directory.CreateDirectory(Companion.Location);
                File.WriteAllText(Path.Combine(Companion.Location, "Money.json"), JsonConvert.SerializeObject(_moneyValues));

                var compound = new NbtCompound("WalletData")
                {
                    new NbtInt("Version", 1)
                };

                var walletsCompound = new NbtCompound("Wallets");
                compound.Add(walletsCompound);

                var walletFile = new NbtFile(compound);
                walletFile.SaveToFile(Path.Combine(Companion.Location, "Money.nbt"), NbtCompression.GZip);
            }
        }

        #endregion Constructor

        #region Functions

        /// <summary>
        /// Saves the current money used by the peds.
        /// </summary>
        internal void Save()
        {
            Directory.CreateDirectory(Companion.Location);
            File.WriteAllText(Path.Combine(Companion.Location, "Money.json"), JsonConvert.SerializeObject(_moneyValues));

            var compound = new NbtCompound("WalletData")
            {
                new NbtInt("Version", 1)
            };

            var walletsCompound = new NbtCompound("Wallets");
            // ReSharper disable once ForeachCanBePartlyConvertedToQueryUsingAnotherGetEnumerator
            foreach (var money in _moneyValues)
            {
                var tempCompound = new NbtCompound(money.Key.ToString())
                {
                    new NbtInt("Money", money.Value)
                };
                walletsCompound.Add(tempCompound);
            }

            compound.Add(walletsCompound);

            var walletFile = new NbtFile(compound);
            walletFile.SaveToFile(Path.Combine(Companion.Location, "Money.nbt"), NbtCompression.GZip);
        }

        #endregion Functions
    }
}