﻿using Newtonsoft.Json;

namespace PlayerCompanion
{
    /// <summary>
    /// The configuration of the mod.
    /// </summary>
    public class Configuration
    {
        /// <summary>
        /// Gets or sets a value indicating whether inventory function should use menu.
        /// </summary>
        [JsonProperty("use_menu")]
        public bool UseMenu { get; set; } = true;

        /// <summary>
        /// Gets or sets a value indicating whether the inventory should be shared between peds.
        /// </summary>
        [JsonProperty("shared_inventory")]
        public bool SharedInventory { get; set; } = false;
    }
}