﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using GTA;
using GTA.Native;
using GTA.UI;
using Newtonsoft.Json;

namespace PlayerCompanion
{
    /// <summary>
    /// Manages the Colors on the HUD and Radar/Map for the Players.
    /// </summary>
    public class ColorManager
    {
        #region Fields

        // ReSharper disable once InconsistentNaming
        private static readonly ColorConverter colorConverter = new ColorConverter();

        private readonly Color _colorM = Color.FromArgb(255, 101, 180, 212);
        private readonly Color _colorF = Color.FromArgb(255, 171, 237, 171);
        private readonly Color _colorT = Color.FromArgb(255, 255, 163, 87);
        private readonly Dictionary<Model, Color> _colors = new Dictionary<Model, Color>();

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or Sets the color based on the current Ped Model.
        /// </summary>
        public Color Current
        {
            get => this[Game.Player.Character.Model];
            set => this[Game.Player.Character.Model] = value;
        }

        /// <summary>
        /// Gets or Sets the color for the specified Ped.
        /// </summary>
        /// <param name="model">The Model of the ped.</param>
        /// <returns>The Color used for the specific ped.</returns>
        public Color this[Model model]
        {
            get
            {
                if (_colors.ContainsKey(model))
                {
                    return _colors[model];
                }
                else if (model == PedHash.Michael)
                {
                    return _colorM;
                }
                else if (model == PedHash.Franklin)
                {
                    return _colorF;
                }
                else if (model == PedHash.Trevor)
                {
                    return _colorT;
                }
                else
                {
                    return default;
                }
            }
            set
            {
                _colors[model] = value;
                Save();
                if (Game.Player.Character.Model == model)
                {
                    Apply(value);
                }
            }
        }

        #endregion Properties

        #region Constructor

        internal ColorManager()
        {
            var file = Path.Combine(Companion.Location, "Colors.json");
            // If the file with colors exists, load it
            if (File.Exists(file))
            {
                var contents = File.ReadAllText(file);
                try
                {
                    var values = JsonConvert.DeserializeObject<Dictionary<string, Color>>(contents, colorConverter);
                    // ReSharper disable once PossibleNullReferenceException
                    foreach (var pair in values)
                    {
                        var model = int.TryParse(pair.Key, out var number) ? new Model(number) : new Model(pair.Key);
                        _colors[model] = pair.Value;
                    }
                }
                catch (JsonSerializationException e)
                {
                    Notification.Show($"~r~Error~s~: Unable to load Colors: {e.Message}");
                }
            }
            // Otherwise, write an empty dictionary
            else
            {
                Directory.CreateDirectory(Companion.Location);
                File.WriteAllText(file, "{}");
            }
        }

        #endregion Constructor

        #region Functions

        /// <summary>
        /// Saves the current money used by the peds.
        /// </summary>
        internal void Save()
        {
            Directory.CreateDirectory(Companion.Location);
            File.WriteAllText(Path.Combine(Companion.Location, "Colors.json"), JsonConvert.SerializeObject(_colors, colorConverter));
        }

        /// <summary>
        /// Restores the colors to their default values.
        /// </summary>
        internal void RestoreDefault()
        {
            // Light
            Function.Call(Hash.REPLACE_HUD_COLOUR_WITH_RGBA, 143, 101, 180, 212, 255); // M
            Function.Call(Hash.REPLACE_HUD_COLOUR_WITH_RGBA, 144, 171, 237, 171, 255); // F
            Function.Call(Hash.REPLACE_HUD_COLOUR_WITH_RGBA, 145, 255, 163, 87, 255); // T
            // Dark
            Function.Call(Hash.REPLACE_HUD_COLOUR_WITH_RGBA, 153, 72, 103, 116, 255); // M
            Function.Call(Hash.REPLACE_HUD_COLOUR_WITH_RGBA, 154, 85, 118, 85, 255); // F
            Function.Call(Hash.REPLACE_HUD_COLOUR_WITH_RGBA, 155, 127, 81, 43, 255); // T
        }

        /// <summary>
        /// Applies a color in the HUD temporarily:
        /// </summary>
        /// <param name="color">The color to apply.</param>
#pragma warning disable CA1822 // Mark members as static

        public void Apply(Color color)
#pragma warning restore CA1822 // Mark members as static
        {
            // Light
            Function.Call(Hash.REPLACE_HUD_COLOUR_WITH_RGBA, 143, (int)color.R, (int)color.G, (int)color.B, (int)color.A); // M
            Function.Call(Hash.REPLACE_HUD_COLOUR_WITH_RGBA, 144, (int)color.R, (int)color.G, (int)color.B, (int)color.A); // F
            Function.Call(Hash.REPLACE_HUD_COLOUR_WITH_RGBA, 145, (int)color.R, (int)color.G, (int)color.B, (int)color.A); // T
            // Dark
            Function.Call(Hash.REPLACE_HUD_COLOUR_WITH_RGBA, 153, (int)color.R, (int)color.G, (int)color.B, (int)color.A); // M
            Function.Call(Hash.REPLACE_HUD_COLOUR_WITH_RGBA, 154, (int)color.R, (int)color.G, (int)color.B, (int)color.A); // F
            Function.Call(Hash.REPLACE_HUD_COLOUR_WITH_RGBA, 155, (int)color.R, (int)color.G, (int)color.B, (int)color.A); // T
        }

        /// <summary>
        /// Checks if the Ped Model has a custom color set.
        /// </summary>
        /// <param name="model">The Ped Model to check.</param>
        public bool HasCustomColor(Model model) => _colors.ContainsKey(model);

        #endregion Functions
    }
}