﻿namespace PlayerCompanion
{
    /// <summary>
    /// Represents the event triggered when an Inventory Item is changed.
    /// </summary>
    /// <param name="sender">The Inventory that triggered the event.</param>
    /// <param name="e">The Item information.</param>
    public delegate void ItemChangedEventHandler(object sender, ItemChangedEventArgs e);

    /// <summary>
    /// Occurs when the information of an item was changed.
    /// </summary>
    public class ItemChangedEventArgs
    {
        #region Properties

        /// <summary>
        /// Gets the associated with this event.
        /// </summary>
        public Item Item { get; }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemChangedEventArgs"/> class.
        /// </summary>
        /// <param name="item">The item that was Changed.</param>
        public ItemChangedEventArgs(Item item)
        {
            Item = item;
        }

        #endregion Constructor
    }
}