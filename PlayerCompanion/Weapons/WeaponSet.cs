﻿using System.Collections.Generic;
using System.IO;
using GTA;
using GTA.Native;
using Newtonsoft.Json;

// ReSharper disable once CheckNamespace
namespace PlayerCompanion
{
    /// <summary>
    /// Represents the weapons owned by a player.
    /// </summary>
    public class WeaponSet
    {
        #region Properties

        /// <summary>
        /// The weapons that are part of this Set.
        /// </summary>
        [JsonProperty("weapons")]
        public List<WeaponInfo> Weapons = new List<WeaponInfo>();

        #endregion Properties

        #region Functions

        /// <summary>
        /// Updates the values of the Set from the Current Player.
        /// </summary>
        public void Populate()
        {
            Weapons.Clear();
            // ReSharper disable once ForeachCanBePartlyConvertedToQueryUsingAnotherGetEnumerator
            foreach (var hash in WeaponManager.sets.Keys)
            {
                if (hash == WeaponHash.Unarmed || hash == WeaponHash.Parachute)
                {
                    continue;
                }

                if (!Function.Call<bool>(Hash.HAS_PED_GOT_WEAPON, Game.Player.Character, hash, false)) continue;
                var info = new WeaponInfo(hash);
                Weapons.Add(info);
            }
        }

        /// <summary>
        /// Saves the current Weapon Set.
        /// </summary>
        /// <param name="model">The Model to save as.</param>
        public void Save(Model model)
        {
            var file = Path.Combine(Companion.Location, "Weapons", $"{model.Hash}.json");
            Directory.CreateDirectory(Path.Combine(Companion.Location, "Weapons"));
            File.WriteAllText(file, JsonConvert.SerializeObject(this));
        }

        /// <summary>
        /// Applies this Weapon Set to the Player.
        /// </summary>
        public void Apply()
        {
            foreach (var weapon in Weapons)
            {
                weapon.Apply();
            }
        }

        #endregion Functions
    }
}