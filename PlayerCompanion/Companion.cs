﻿using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using GTA;
using GTA.Native;
using GTA.UI;
using JetBrains.Annotations;
using LemonUI;
using LemonUI.Elements;
using Newtonsoft.Json;
using Control = GTA.Control;
using Screen = LemonUI.Screen;

// ReSharper disable MemberCanBeMadeStatic.Local

namespace PlayerCompanion
{
    /// <summary>
    /// Main class for managing the player information.
    /// </summary>
    [UsedImplicitly]
    public class Companion : Script
    {
        #region Fields

        private static Model _lastModel = default;
        private static int _nextWeaponUpdate = 0;
        private static int _nextWeaponSave = 0;

        internal static readonly ScaledText MoneyTotal = new ScaledText(PointF.Empty, "$0", 0.65f, GTA.UI.Font.Pricedown)
        {
            Alignment = Alignment.Right,
            Outline = true
        };

        internal static readonly ScaledText MoneyChange = new ScaledText(PointF.Empty, "$0", 0.5f, GTA.UI.Font.Pricedown)
        {
            Alignment = Alignment.Right,
            Outline = true
        };

#pragma warning disable S2223 // Non-constant static fields should not be visible
        internal static int DrawUntil = 0;
#pragma warning restore S2223 // Non-constant static fields should not be visible

        internal static readonly ObjectPool Pool = new ObjectPool();
        internal static readonly InventoryMenu Menu = new InventoryMenu();

#pragma warning disable S2223 // Non-constant static fields should not be visible

        // ReSharper disable once AssignNullToNotNullAttribute
        internal static string Location = Path.Combine(new Uri(Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase)).LocalPath, "PlayerCompanion");

        internal static Configuration Config = null;
#pragma warning restore S2223 // Non-constant static fields should not be visible

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets a value indicating whether this script is currently ready.
        /// </summary>
        public static bool IsReady { get; private set; } = false;

        /// <summary>
        /// Gets an instance of <see cref="Wallet"/>, which can be used for managing player money.
        /// </summary>
        public static Wallet Wallet { get; } = new Wallet();

        /// <summary>
        /// Gets an instance of <see cref="ColorManager"/> which manages the colors in the HUD and Radar.
        /// </summary>
        public static ColorManager Colors { get; } = new ColorManager();

        /// <summary>
        /// Gets an instance of <see cref="InventoryManager"/> which manages the inventory.
        /// </summary>
        public static InventoryManager Inventories { get; } = new InventoryManager();

        /// <summary>
        /// Gets an instance of <see cref="WeaponManager"/> which manages the weapons.
        /// </summary>
        public static WeaponManager Weapons { get; } = new WeaponManager();

        #endregion Properties

        #region Events

        /// <summary>
        /// Event Triggered when PlayerCompanion is ready to work.
        /// </summary>
        public static event EventHandler Ready;

        #endregion Events

        #region Constructor

        /// <summary>
        /// Creates a new PlayerCompanion Script.
        /// Please note that this can only be done by SHVDN.
        /// </summary>
        public Companion()
        {
            // Get the assembly that called and it's name
            var assembly = Assembly.GetCallingAssembly();
            var name = assembly.GetName();
            // If the name is not ScriptHookVDotNet, raise an exception
            if (name.Name != "ScriptHookVDotNet")
            {
                throw new InvalidOperationException($"PlayerCompanion can only be started by ScriptHookVDotNet (it was called from '{name.Name}').");
            }

            // If there is a configuration file, load it
            var path = Path.Combine(Location, "Config.json");
            if (File.Exists(path))
            {
#pragma warning disable S3010 // Static fields should not be updated in constructors
                Config = JsonConvert.DeserializeObject<Configuration>(File.ReadAllText(path));
#pragma warning restore S3010 // Static fields should not be updated in constructors
            }
            // Otherwise, create a new one and save it
            else
            {
                Config = new Configuration();
                Directory.CreateDirectory(Location);
                File.WriteAllText(Path.Combine(Location, path), JsonConvert.SerializeObject(Config));
            }

            // Add the items to the pool
            Pool.Add(Menu);
            // Finally, add the events that we need
            Tick += Companion_Tick;
            Aborted += Companion_Aborted;
            KeyDown += Companion_KeyDown;
            Inventories.ItemAdded += Inventories_ItemAdded;
            Inventories.ItemRemoved += Inventories_ItemRemoved;
            // And set the money text
            MoneyTotal.Text = $"${Wallet.Money}";
        }

        #endregion Constructor

        #region Events

        // ReSharper disable once MemberCanBeMadeStatic.Local
        private void Companion_Tick(object sender, EventArgs e)
        {
            // If PlayerCompanion is not ready to work, perform the initialization
            if (!IsReady)
            {
                Inventories.PopulateItems();
                IsReady = true;
                // Fix S4220: Will no longer transfer itself!
                Ready?.Invoke(sender: null, e: EventArgs.Empty);
                return;
            }

            // Process the menu pool
            Pool.Process();

            // If the player entered the inventory cheat, toggle it
            if (Game.WasCheatStringJustEntered("inventory"))
            {
                Menu.Visible = !Menu.Visible;
            }

            // Hide the vanilla Money text
            Hud.HideComponentThisFrame(HudComponent.Cash);
            Hud.HideComponentThisFrame(HudComponent.CashChange);
            Function.Call(Hash.DISPLAY_CASH, false);

            var switchOpen = Function.Call<bool>(Hash.THEFEED_IS_PAUSED) && Game.IsControlPressed(Control.CharacterWheel);
            var starsShown = Hud.IsComponentActive(HudComponent.WantedStars);
            var ammoShown = Hud.IsComponentActive(HudComponent.WeaponIcon);

            Screen.SetElementAlignment(GFXAlignment.Right, GFXAlignment.Top);
            var position = Screen.GetRealPosition(0, (ammoShown ? 35 : 0) + (starsShown ? 40 : 0));
            Screen.ResetElementAlignment();

            // If the player is pressing alt, draw the total money
            if (switchOpen)
            {
                MoneyTotal.Position = position;
                MoneyTotal.Draw();
            }
            // If the player has received or got money removed
            if (DrawUntil >= Game.GameTime)
            {
                if (switchOpen)
                {
                    position.Y += 40;
                }

                MoneyChange.Position = new PointF(position.X, position.Y);
                MoneyChange.Draw();
            }

            // If the Player Ped Model has been changed, make the required updates
            if (Game.Player.Character.Model != _lastModel)
            {
                if (Colors.HasCustomColor(Game.Player.Character.Model))
                {
                    Colors.Apply(Colors.Current);
                }
                else
                {
                    switch ((PedHash)Game.Player.Character.Model)
                    {
                        case PedHash.Franklin:
                        case PedHash.Michael:
                        case PedHash.Trevor:
                            Colors.RestoreDefault();
                            break;

                        default:
                            Notification.Show($"~o~Warning~s~: Ped Model {Game.Player.Character.Model.Hash} does not has a Color set!");
                            Colors.Apply(Color.LightGray);
                            break;
                    }
                }
                if (_lastModel != default)
                {
                    Weapons[_lastModel]?.Save(_lastModel);
                }
                Weapons.Current?.Apply();
                Inventories.Load(Game.Player.Character.Model);

                Menu.ReloadItems();

                _lastModel = Game.Player.Character.Model;
                _nextWeaponUpdate = Game.GameTime + (1000 * 5);
            }

            // If is time to update the weapons that the player has
            if (_nextWeaponUpdate <= Game.GameTime)
            {
                Weapons.Current?.Populate();
                _nextWeaponUpdate = Game.GameTime + (1000 * 5);
            }

            // If is time to save the weapons
            if (_nextWeaponSave <= Game.GameTime)
            {
                Weapons.Current?.Save(Game.Player.Character.Model);
                _nextWeaponSave = Game.GameTime + (1000 * 30);
            }
        }

        // ReSharper disable once MemberCanBeMadeStatic.Local
        private void Companion_Aborted(object sender, EventArgs e)
        {
            // Do the required cleanup tasks
            Colors.RestoreDefault();
            // Restore the money on the screen
            Function.Call(Hash.DISPLAY_CASH, true);
            // And save the user weapons
            Weapons.Current?.Populate();
            Weapons.Current?.Save(Game.Player.Character.Model);
        }

        private void Companion_KeyDown(object sender, KeyEventArgs e)
        {
            // ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
            switch (e.KeyCode)
            {
                case Keys.I:
                    Menu.Visible = !Menu.Visible;
                    break;
            }
        }

        private void Inventories_ItemAdded(object sender, ItemChangedEventArgs e)
        {
            Menu.Add(new InventoryItem(e.Item));
        }

        private void Inventories_ItemRemoved(object sender, ItemChangedEventArgs e)
        {
            Menu.Remove(item => item is InventoryItem ii && ii.Item == e.Item);
        }

        #endregion Events
    }
}