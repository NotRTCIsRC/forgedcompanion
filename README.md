# ForgedCompanion

![AppVeyor](https://ci.appveyor.com/api/projects/status/bitbucket/relaperdev/forgedcompanion?svg=true) ![Bitbucket open issues](https://img.shields.io/bitbucket/issues/relaperdev/forgedcompanion) ![Bitbucket open pull requests](https://img.shields.io/bitbucket/pr/relaperdev/forgedcompanion)PlayerCompanion is a mod for Grand Theft Auto V that aims to extend the gameplay features of the game by adding some extra tools for developers and users alike:

* An Inventory system that can be shared across different mods, where any developer can add and consume Items
* Persistant Storage of Weapons for Addon Peds (including MK2 Weapons)
* Wallet for storing the Money of Addon Peds (dependant on mod compatibility)
* Custom HUD Colors for Addon Peds (customizable by the end user)

The main target of the mod is increase the replay value of Grand Theft Auto V by using PlayerCompanion in combination with other mods to increase the things that the user can do in the game.

## Download

* [GitHub](https://github.com/justalemon/PlayerCompanion/releases)
* [5mods](https://www.gta5-mods.com/scripts/playercompanion)
* [AppVeyor](https://ci.appveyor.com/project/justalemon/playercompanion) (experimental)

## Installation

Just drag and drop all of the files inside of the 7zip into your **scripts** directory.

## Usage

You can open the built in inventory menu by pressing I on your keyboard or entering the word "inventory" in the cheat input field.

Check [the wiki](https://github.com/justalemon/PlayerCompanion/wiki) for the usage instructions of the other functions that are part of PlayerCompanion.
